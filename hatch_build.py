"""Custom build hook

Nicked from https://github.com/facelessuser/Rummage/blob/master/hatch_build.py
"""
import os
import shutil
import logging
from subprocess import check_call

from hatchling.builders.hooks.plugin.interface import BuildHookInterface

_logger = None

HERE = os.path.abspath(os.path.dirname(__file__))
SHARE = os.path.join(HERE, 'share', 'novnc')

IS_REPO = os.path.exists(os.path.join(HERE, '.git'))


def _get_log():
    global _logger  # noqa
    if _logger:
        return _logger  # type:ignore[unreachable]
    _logger = logging.getLogger(__name__)
    _logger.setLevel(logging.INFO)
    logging.basicConfig(level=logging.INFO)
    return _logger


class CustomBuildHook(BuildHookInterface):
    """Custom build hook for noVNC."""

    PLUGIN_NAME = 'custom'

    # Custom vars
    log = _get_log()
    novnc_dir = SHARE
    pkg_share_dir = os.path.join(HERE, 'jupyter_novnc_proxy', 'novnc')
    novnc_commid_id = '3553a451d8b9cf566232b7de2e764861c57a0e9a'

    def should_clone(self):
        if not os.path.exists(self.novnc_dir):
            return True
        else:
            False

    def run(self):
        if not self.should_clone():
            self.log.info('noVNC already cloned')
        else:
            self.log.info('Installing noVNC web content')
            check_call(
                f'git clone https://github.com/novnc/noVNC.git {self.novnc_dir} '
                f'&& cd {self.novnc_dir} && git checkout {self.novnc_commid_id}',
                cwd=HERE,
                shell=True,
            )
        
        # Copy patched noVNC files to cloned repo
        self.log.info('Copying patched HTML and JS files')
        shutil.copy(
            os.path.join(self.pkg_share_dir, 'vnc_lite.html'),
            os.path.join(self.novnc_dir, 'vnc_lite.html')
        )
        shutil.copy(
            os.path.join(self.pkg_share_dir, 'app', 'ui.js'),
            os.path.join(self.novnc_dir, 'app', 'ui.js')
        )
        
    def initialize(self, version, build_data):
        """Build localization files."""

        # Clone noVNC repo
        self.run()
        return
