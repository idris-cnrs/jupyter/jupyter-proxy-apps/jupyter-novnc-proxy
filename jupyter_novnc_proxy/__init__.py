# Copyright 2022 IDRIS / jupyter
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
import re
import socket
import shutil
import stat
import logging
import sys
import pexpect
import secrets

from typing import Any
from typing import Dict

from jinja2 import Template

from ._version import __version__


def get_logger(name):
    """Configure logging"""
    logger = logging.getLogger(name)
    if not logger.handlers:
        # Prevent logging from propagating to the root logger
        logger.propagate = 0
        console = logging.StreamHandler()
        logger.addHandler(console)
        formatter = logging.Formatter(
            '[%(levelname).1s %(asctime)s.%(msecs)03d %(module)s '
            '%(funcName)s:%(lineno)d] %(message)s',
            datefmt='%Y-%m-%d %H:%M:%S'
        )
        console.setFormatter(formatter)
    return logger


def setup_vnc_password(config_dir, vnc_passwd_executable='/opt/TurboVNC/bin/vncpasswd'):
        """Setup password for vnc"""
        # First check if passwd executebale exists
        if not os.path.exists(vnc_passwd_executable):
            return

        # generate file with random one-time-password
        vnc_passwd = os.environ.get('NOVNC_PASSWD', str(secrets.token_hex(16)))

        # vnc password command
        try:
            p = pexpect.spawn(vnc_passwd_executable)
            p.expect("Password:")
            p.sendline(vnc_passwd)
            p.expect("Verify:")
            p.sendline(vnc_passwd)
            p.expect("view-only password (y/n)?")
            p.sendline("n")
            p.sendline()
        except:
            raise RuntimeError(
                "Setting up VNC password failed with error"
            )

        # Save the password in plain text as well
        with open(os.path.join(config_dir, 'idris_pass'), 'w') as f:
            f.write(f'Password VNC:   {vnc_passwd}\n')
        return


def get_free_port(min_port=5930, max_port=5951):
    """Get free port within range"""
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    for p in range(min_port, max_port):
        try:
            sock.bind(('', p))
            sock.close()
            return p
        except OSError:
            pass
    raise IOError(f'no available free ports within {min_port}-{max_port}')


def setup_novnc() -> Dict[str, Any]:
    """ Setup commands and return a dictionary compatible
        with jupyter-server-proxy.
    """

    # Set logging
    logger = get_logger(__name__)
    logger.setLevel(logging.INFO)

    # websockify executable name
    websockify_executable = 'websockify'

    # Get home dir
    home_dir = os.path.expanduser('~')
    # Config dir path
    config_dir = os.path.join(home_dir, '.vnc')

    # Ensure config_dir exists
    if not os.path.exists(config_dir):
        os.makedirs(config_dir)

    # Path to noVNC web root
    novnc_web_root = os.path.join(sys.prefix, 'share', 'novnc')
    # XDG runtime dir
    novnc_xdg_runtime_dir = os.path.join(config_dir, '.xdg')

    # Get a free port for VNC
    vnc_port = int(get_free_port())

    # Setup vnc password
    setup_vnc_password(config_dir=config_dir)

    def _create_xstartup_config(vncserver, window_manager):
        """Create a xstartup config file"""
        # Make sure config dir exists
        if not os.path.exists(config_dir):
            os.makedirs(config_dir, exist_ok=True)
        config_content = Template("""#!/bin/sh
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
xsetroot -solid '#222E45'
vncconfig -iconic &
{% if window_manager == 'minimal' %}
/usr/bin/mwm &
{% elif window_manager == 'gnome' %}
/usr/bin/dbus-launch --exit-with-session gnome-session &
{% elif window_manager == 'kde' %}
/usr/bin/startkde &
{% endif %}
xterm -geometry 80x24+10+10 -ls -title "$VNCDESKTOP Desktop" &
""").render(window_manager=window_manager)

        # Set config file path based on type of vncserver
        if vncserver == 'turbovnc':
            config_file_path = os.path.join(config_dir, 'xstartup.turbovnc')
        else:
            config_file_path = os.path.join(config_dir, 'xstartup')

        # Write content to config file
        with open(config_file_path, 'w') as f:
            f.write(config_content)

        # Make it executable
        st = os.stat(config_file_path)
        os.chmod(config_file_path, st.st_mode | stat.S_IEXEC)

        # Make sure XDG_RUNTIME_DIR exists
        os.makedirs(novnc_xdg_runtime_dir, exist_ok=True)

    def _get_icon_path():
        """Get the icon path"""
        return os.path.join(
            os.path.dirname(os.path.abspath(__file__)), 'icons',
            'novnc-logo.svg'
        )

    def _get_path_info():
        """Get custom URL launcher with auth parmeters"""
        base_path = '/novnc/vnc.html'
        password_file_path = os.path.join(config_dir, 'idris_pass')

        # If password file does not exist, return with generic URL
        if not os.path.exists(password_file_path):
            return base_path

        # If password file exists, read password and add it to URL params
        passwd_file_contents = open(password_file_path, 'r').read()
        match = re.search(
            r'Password VNC:\s*([a-zA-Z0-9]+$)', passwd_file_contents
        )
        if match:
            return f'{base_path}#password={match.group(1)}'

        # If password file is tempered, again return generic URL
        return base_path

    def _novnc_command(port, args):
        """Callable that we will pass to sever proxy to spin up
        no vnc client"""
        # Check if websockify executable is available
        if not shutil.which(websockify_executable):
            raise FileNotFoundError(
                f'{websockify_executable} executable which is needed for novnc '
                 'client not found.'
            )

        # Get server and window manager arguments if provided
        # Default arguments
        args_dict = {
            'server': 'turbovnc',
            'window': 'gnome',
            'geometry': '1440x900',
        }
        for arg in [
            '--geometry', '-g', '--window-manager', '-w', '--server', '-s'
            ]:
            if arg in args:
                idx = args.index(arg)
                if arg in ['--geometry', '-g']:
                    args_dict['geometry'] = args[idx + 1]
                elif arg in ['--server', '-s']:
                    args_dict['server'] = args[idx + 1]

        # Make vnc command
        if args_dict.get('server') == 'tigervnc':
            vnc_executable = '/usr/bin/vncserver'
        else:
            vnc_executable = '/opt/TurboVNC/bin/vncserver'

        # Raise exception if vnc_executable/vnc_passwd executable not found
        if not os.path.exists(vnc_executable):
            raise FileNotFoundError(
                f'{vnc_executable} executable not found'
            )

        # Create vnc config based on window manager
        _create_xstartup_config(
            args_dict.get('server'), args_dict.get('window')
        )

        # Get display number with 5900 as reference
        display_number = vnc_port - 5900

        # Make vnc command
        vnc_command = [
            vnc_executable, f':{display_number}',
            '-geometry', f"{args_dict.get('geometry')}",
        ]

        # Add FQDN of internal interface as interface arg
        hostname = os.environ.get('SLURMD_NODENAME', '')
        domain = os.environ.get('JUPYTERHUB_INTERNAL_DOMAIN', '')
        if hostname and domain:
            vnc_command += ['-interface', f'{hostname}.{domain}']

        # Generate custom script to trap signal to websockify to kill vncserver
        # too
        script_template = """#!/bin/bash
exit_script() {{
    {vnc_executable} -kill :{display_number}
    get_child_pids $$
    trap - SIGTERM # clear the trap
    kill -INT $CPIDS # Sends SIGTERM to child/sub processes
    exit 0
}}

function get_child_pids() {{
    pids=`pgrep -P $1|xargs`
    for pid in $pids;
    do
        CPIDS="$CPIDS $pid"
        get_child_pids $pid
    done
}}

trap exit_script SIGTERM

CPIDS=''

{websockify_executable} "$@" &
wait
""".format(
    websockify_executable=websockify_executable,
    vnc_executable=vnc_executable,
    display_number=display_number
)

        # Fall back root directory. By default we use JOBSCRATCH to place ephermal
        # scripts. If this is not available we need to have a smart fallback
        # option to take different users and different JupyterLab instances
        # into account.
        # Fallback to fallback is /tmp/$USER
        fallback_scratch_dir_prefix = os.environ.get(
            'JUPYTER_FILES_ROOT', default=os.path.join(
                '/tmp', os.environ.get('USER')
            )
        )
        # Root directory to save the code server wrapper
        scratch_dir_perfix = os.environ.get(
            'JOBSCRATCH', default=fallback_scratch_dir_prefix
        )
        scratch_dir = os.path.join(
            scratch_dir_perfix,
            'bin',
            os.environ.get('JUPYTERHUB_SERVER_NAME', default='jupyterlab')
        )
        # Check if scratch dir exists and create one if it does not
        if not os.path.exists(scratch_dir):
            os.makedirs(scratch_dir, exist_ok=True)

        # Path to websockify wrapper
        websockify_wrapper = os.path.join(
            scratch_dir, 'websockify_wrapper.sh'
        )
        # Write wrapper script to directory
        with open(websockify_wrapper, 'w') as f:
            f.write(script_template)
        # Make it executable
        st = os.stat(websockify_wrapper)
        os.chmod(websockify_wrapper, st.st_mode | stat.S_IEXEC)

        # Make complete command adding websockify cmd
        cmd_args = [
            websockify_wrapper, '-v', '--web', novnc_web_root, '--heartbeat',
            '30', '--wrap-mode', 'ignore', str(vnc_port), '--'
        ] + vnc_command

        logger.info(
            'NoVNC server/client will be launched via websockify with '
            'arguments %s', cmd_args
        )
        return cmd_args

    return {
        'command': _novnc_command,
        'port': vnc_port,
        'absolute_url': False,
        'timeout': 300,
        'new_browser_tab': True,
        'environment': {'XDG_RUNTIME_DIR': novnc_xdg_runtime_dir},
        'launcher_entry': {
            'enabled': True,
            'title': 'noVNC',
            'num_instances': 1,
            'path_info': _get_path_info(),
            'icon_path': _get_icon_path(),
            'category': 'Applications',
        }
    }
